Pod::Spec.new do |spec|
  spec.name = "BLCoreFramework"
  spec.version = "0.1.2"
  spec.summary = "BilineDEV iOS Core Framework."
  spec.homepage = "https://bitbucket.com/biline_dev/BLCoreFramework-iOS"
  spec.license = { type: 'MIT', file: 'LICENSE' }
  spec.authors = { "BilineDEV Mobile" => 'bilinedev.mobile@gmail.com' }

  spec.platform = :ios, "9.0"
  spec.requires_arc = true
  spec.source = { git: "https://bitbucket.com/biline_dev/BLCoreFramework-iOS.git", tag: "v#{spec.version}", submodules: true }
  spec.source_files = "BLCoreFramework/**/*.{h,swift}"
end
