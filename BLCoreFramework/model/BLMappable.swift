//
//  BLMappable.swift
//  BLCoreFramework
//
//  Created by Muhammad Ikhsan on 8/10/17.
//  Copyright © 2017 BilineDEV. All rights reserved.
//

import Foundation

public protocol BLMappable {
//    init?(dictionary: [String: Any]?)
    func dictionaryValue() -> [String: Any]
}
