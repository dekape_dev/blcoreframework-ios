//
//  BLRoundedButton.swift
//  BLCoreFramework
//
//  Created by Muhammad Ikhsan on 15/11/17.
//  Copyright © 2017 BilineDEV. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable
class BLRoundedButton: UIButton {
    
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            setupViews()
        }
    }
    
    @IBInspectable var borderColor: UIColor = .clear {
        didSet {
            setupViews()
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            setupViews()
        }
    }
    
    @IBInspectable var dashedBorder: Bool = false {
        didSet {
            setupViews()
        }
    }
    
    @IBInspectable var imageInset: CGFloat = 0 {
        didSet {
            setupViews()
        }
    }
    
    @IBInspectable var tintedImage: Bool = false {
        didSet {
            setupViews()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupViews()
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        setupViews()
    }
    
    func setupViews() {
        imageEdgeInsets = UIEdgeInsets(top: imageInset, left: imageInset, bottom: imageInset, right: imageInset)
        imageView?.contentMode = .scaleAspectFit
        imageView?.autoresizesSubviews = true
        if tintedImage {
            imageView?.image = imageView?.image?.tint(with: tintColor)
        }
        
        if dashedBorder {
            layer.borderWidth = 0
            
            let shapeBorder = CAShapeLayer()
            shapeBorder.strokeColor = borderColor.cgColor
            shapeBorder.fillColor = UIColor.clear.cgColor
            shapeBorder.cornerRadius = cornerRadius
            shapeBorder.masksToBounds = cornerRadius > 0
            shapeBorder.lineWidth = borderWidth
            shapeBorder.lineDashPattern = [5, 3]
            shapeBorder.frame = bounds
            let roundRect = bounds.insetBy(dx: borderWidth, dy: borderWidth)
            shapeBorder.path = UIBezierPath.init(roundedRect: roundRect, cornerRadius: cornerRadius).cgPath
            layer.addSublayer(shapeBorder)
        } else {
            layer.cornerRadius = cornerRadius
            layer.borderColor = borderColor.cgColor
            layer.borderWidth = borderWidth
            layer.masksToBounds = cornerRadius > 0
        }
        
        setNeedsDisplay()
    }
}
