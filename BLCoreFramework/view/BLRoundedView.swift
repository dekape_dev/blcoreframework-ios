//
//  BLRoundedView.swift
//  BLCoreFramework
//
//  Created by Muhammad Ikhsan on 14/11/17.
//  Copyright © 2017 BilineDEV. All rights reserved.
//

import Foundation

@IBDesignable
class BLRoundedView: UIView {
    
    @IBInspectable var cornerRadius: CGFloat = 0.0 {
        didSet {
            setupViews()
        }
    }
    
    @IBInspectable var topLeft: Bool = true {
        didSet {
            setupViews()
        }
    }
    
    @IBInspectable var topRight: Bool = true {
        didSet {
            setupViews()
        }
    }
    
    @IBInspectable var bottomLeft: Bool = true {
        didSet {
            setupViews()
        }
    }
    
    @IBInspectable var bottomRight: Bool = true {
        didSet {
            setupViews()
        }
    }
    
    @IBInspectable var borderColor: UIColor = .clear {
        didSet {
            setupViews()
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 0.0 {
        didSet {
            setupViews()
        }
    }
    
    @IBInspectable var dashedBorder: Bool = false {
        didSet {
            setupViews()
        }
    }
    
    let shapeBorder = CAShapeLayer()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupViews()
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        setupViews()
    }
    
    func setupViews() {
        if dashedBorder {
            layer.borderWidth = 0
            
            shapeBorder.strokeColor = borderColor.cgColor
            shapeBorder.fillColor = UIColor.clear.cgColor
            shapeBorder.cornerRadius = cornerRadius
            shapeBorder.masksToBounds = cornerRadius > 0
            shapeBorder.lineWidth = borderWidth
            shapeBorder.lineDashPattern = [5, 3]
            
            layer.addSublayer(shapeBorder)
        } else {
            if topRight && topLeft && bottomLeft && bottomRight {
                layer.cornerRadius = cornerRadius
                layer.borderColor = borderColor.cgColor
                layer.borderWidth = borderWidth
                layer.masksToBounds = cornerRadius > 0
            } else {
                var rectCorners: UIRectCorner = UIRectCorner()
                if topLeft { rectCorners.insert(.topLeft) }
                if topRight { rectCorners.insert(.topRight) }
                if bottomLeft { rectCorners.insert(.bottomLeft) }
                if bottomRight { rectCorners.insert(.bottomRight) }
                let path = UIBezierPath(roundedRect:bounds,
                                        byRoundingCorners:rectCorners,
                                        cornerRadii: CGSize(width: 20, height:  20))
                let maskLayer = CAShapeLayer()
                maskLayer.path = path.cgPath
                layer.mask = maskLayer
                layer.borderColor = borderColor.cgColor
                layer.borderWidth = borderWidth
                layer.masksToBounds = cornerRadius > 0
            }
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        shapeBorder.frame = bounds
        let roundRect = bounds.insetBy(dx: borderWidth, dy: borderWidth)
        shapeBorder.path = UIBezierPath.init(roundedRect: roundRect, cornerRadius: cornerRadius).cgPath
    }
}
