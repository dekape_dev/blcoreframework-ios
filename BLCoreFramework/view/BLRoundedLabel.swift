//
//  BLRoundedLabel.swift
//  BLCoreFramework
//
//  Created by Muhammad Ikhsan on 15/11/17.
//  Copyright © 2017 BilineDEV. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable
class BLRoundedLabel: UILabel {
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    @IBInspectable var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = newValue > 0
        }
    }
    
    @IBInspectable var borderColor: UIColor {
        get {
            return UIColor(cgColor: layer.borderColor!)
        }
        set {
            layer.borderWidth = 1
            layer.borderColor = newValue.cgColor
        }
    }
}
