//
//  BLLightNavigationController.swift
//  BLCoreFramework
//
//  Created by Muhammad Ikhsan on 11/02/19.
//  Copyright © 2019 BilineDEV. All rights reserved.
//

import Foundation

class BLLightNavigationController: UINavigationController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationBar.shadowImage = UIImage()
        navigationBar.barTintColor = .clear
        navigationBar.tintColor = .white
        navigationBar.titleTextAttributes = [.foregroundColor: UIColor.white]
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}

extension UIViewController {
    func wrappedInLightNVC() -> BLLightNavigationController {
        return BLLightNavigationController(rootViewController: self)
    }
}
