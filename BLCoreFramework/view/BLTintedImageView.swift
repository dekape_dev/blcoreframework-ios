//
//  BLTintedImageView.swift
//  BLCoreFramework
//
//  Created by Muhammad Ikhsan on 11/02/19.
//  Copyright © 2019 BilineDEV. All rights reserved.
//

import UIKit

class BLTintedImageView: UIImageView {
    override func awakeFromNib() {
        super.awakeFromNib()
        
        if let image = self.image {
            self.image = image.tint(with: tintColor)
            setNeedsDisplay()
        }
    }
}
