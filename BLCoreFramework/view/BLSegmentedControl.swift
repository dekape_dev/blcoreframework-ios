//
//  BLSegmentedControl.swift
//  BLCoreFramework
//
//  Created by Muhammad Ikhsan on 11/02/19.
//  Copyright © 2019 BilineDEV. All rights reserved.
//

import UIKit

@IBDesignable
class BLSegmentedControl: UIControl {
    
    var buttons: [UIButton] = []
    var buttonsTitle: [String] = [] {
        didSet {
            setNeedsDisplay()
        }
    }
    var selector: UIView = UIView()
    var lastSelectedIndex: Int = 0
    
    @IBInspectable
    var buttonsTitleWithCommaDivider: String = "" {
        didSet {
            self.buttonsTitle = buttonsTitleWithCommaDivider.components(separatedBy: ",")
            //            setNeedsDisplay()
        }
    }
    
    @IBInspectable
    var font: UIFont = UIFont.systemFont(ofSize: 15) {
        didSet {
            setNeedsDisplay()
        }
    }
    
    @IBInspectable
    var titleColor: UIColor = UIColor(white: 1.0, alpha: 0.7) {
        didSet {
            setNeedsDisplay()
        }
    }
    
    @IBInspectable
    var titleSelectedColor: UIColor = .white {
        didSet {
            setNeedsDisplay()
        }
    }
    
    @IBInspectable
    var selectorColor: UIColor = .lightGray {
        didSet {
            setNeedsDisplay()
        }
    }
    
    @IBInspectable
    var selectorHeight: CGFloat = 3 {
        didSet {
            setNeedsDisplay()
        }
    }
    
    @IBInspectable
    var selectorHeightSameAsRoot: Bool = false {
        didSet {
            setNeedsDisplay()
        }
    }
    
    @IBInspectable
    var dividerColor: UIColor = .gray {
        didSet {
            setNeedsDisplay()
        }
    }
    
    @IBInspectable
    var borderColor: UIColor = .gray {
        didSet {
            setNeedsDisplay()
        }
    }
    
    @IBInspectable
    var borderWidth: CGFloat = 0.0 {
        didSet {
            setNeedsDisplay()
        }
    }
    
    @IBInspectable
    var cornerRadius: CGFloat = 0.0 {
        didSet {
            setNeedsDisplay()
        }
    }
    
    @IBInspectable
    var useDivider: Bool = false {
        didSet {
            setNeedsDisplay()
        }
    }
    
    var dividerWidth: CGFloat = 1.0 {
        didSet {
            setNeedsDisplay()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func draw(_ rect: CGRect) {
        self.subviews.forEach({ $0.removeFromSuperview() })
        guard !buttonsTitle.isEmpty else { return }
        
        if borderWidth > 0 {
            self.layer.borderColor = borderColor.cgColor
            self.layer.borderWidth = borderWidth
        }
        if cornerRadius > 0 {
            self.layer.cornerRadius = cornerRadius
            self.layer.masksToBounds = true
        }
        
        let container = UIView()
        container.frame = CGRect(x: 0, y: 0, width: rect.width, height: rect.height)
        container.isUserInteractionEnabled = true
        
        buttons.removeAll()
        for (index, title) in buttonsTitle.enumerated() {
            let button = UIButton(type: .custom)
            button.setTitle(title, for: .normal)
            button.setTitleColor(titleColor, for: .normal)
            button.setTitleColor(titleSelectedColor, for: .selected)
            button.addTarget(self, action: #selector(buttonControlTapped(_:)), for: .touchUpInside)
            button.frame = calculateFrame(container: container, isDivider: false)
            button.titleLabel?.font = font
            
            self.buttons.append(button)
            container.addSubview(button)
            
            if useDivider && index < buttonsTitle.count - 1 {
                let div = UIView()
                div.backgroundColor = borderColor
                div.frame = calculateFrame(container: container, isDivider: true)
                container.addSubview(div)
            }
        }
        
        addSubview(container)
        container.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        container.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        container.topAnchor.constraint(equalTo: topAnchor).isActive = true
        container.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        
        if selectorHeightSameAsRoot {
            selector.frame = buttons.first!.frame
        } else {
            let refFrame = buttons.first!.frame
            selector.frame = CGRect(x: 0, y: refFrame.height - selectorHeight, width: refFrame.width, height: selectorHeight)
        }
        selector.widthAnchor.constraint(equalToConstant: selector.frame.width).isActive = true
        selector.backgroundColor = selectorColor
        insertSubview(selector, at: 0)
        
        buttons[lastSelectedIndex].isSelected = true
    }
    
    func calculateFrame(container: UIView, isDivider: Bool) -> CGRect {
        let titleCount = CGFloat(buttonsTitle.count)
        let divAvg = dividerWidth * (titleCount - 1) / titleCount
        let width = isDivider ? dividerWidth : frame.width / titleCount - divAvg
        
        let lastView = container.subviews.last
        let x = lastView == nil ? 0 : lastView!.frame.origin.x + lastView!.frame.width
        return CGRect(x: x, y: 0, width: width, height: frame.height)
    }
    
    @objc func buttonControlTapped(_ sender: UIButton) {
        UIView.animate(withDuration: 0.3, animations: {
            if self.selectorHeightSameAsRoot {
                self.selector.frame = sender.frame
            } else {
                let refFrame = sender.frame
                self.selector.frame = CGRect(x: sender.frame.minX, y: refFrame.height - self.selectorHeight, width: refFrame.width, height: self.selectorHeight)
            }
            
            self.buttons[self.lastSelectedIndex].isSelected = false
            sender.isSelected = true
            if let index = self.buttons.index(of: sender) {
                self.lastSelectedIndex = index
            }
            self.sendActions(for: .valueChanged)
        })
    }
}

