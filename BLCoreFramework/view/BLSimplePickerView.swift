//
//  BLSimplePickerView.swift
//  BLCoreFramework
//
//  Created by Muhammad Ikhsan on 11/02/19.
//  Copyright © 2019 BilineDEV. All rights reserved.
//

import UIKit

typealias BLSimplePickerItem = Any

class BLSimplePickerView: UIPickerView, UIPickerViewDelegate, UIPickerViewDataSource {
    
    var source: [BLSimplePickerItem] = [] {
        didSet {
            print(source)
        }
    }
    weak var viewDelegate: BLSimplePickerViewDelegate? {
        didSet {
            self.dataSource = self
            self.delegate = self
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        viewDelegate?.simplePickerDidSelect(picker: self, item: source[row])
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 38
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return source.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return viewDelegate?.simplePickerTitleFor(picker: self, item: source[row])
    }
    
    func pickerView(_ pickerView: UIPickerView, widthForComponent component: Int) -> CGFloat {
        return pickerView.bounds.width
    }
}

protocol BLSimplePickerViewDelegate: NSObjectProtocol {
    
    func simplePickerDidSelect(picker: BLSimplePickerView, item: BLSimplePickerItem)
    func simplePickerTitleFor(picker: BLSimplePickerView, item: BLSimplePickerItem) -> String?
}
