//
//  BLInsetsLabel.swift
//  BLCoreFramework
//
//  Created by Muhammad Ikhsan on 11/02/19.
//  Copyright © 2019 BilineDEV. All rights reserved.
//

import UIKit

class BLInsetsLabel: BLRoundedLabel {
    
    @IBInspectable
    var topInset: CGFloat = 3 {
        didSet {
            setNeedsDisplay()
        }
    }
    @IBInspectable
    var bottomInset: CGFloat = 3 {
        didSet {
            setNeedsDisplay()
        }
    }
    @IBInspectable
    var leftInset: CGFloat = 5 {
        didSet {
            setNeedsDisplay()
        }
    }
    @IBInspectable
    var rightInset: CGFloat = 5 {
        didSet {
            setNeedsDisplay()
        }
    }
    
    override func drawText(in rect: CGRect) {
        let insets: UIEdgeInsets = UIEdgeInsets(top: topInset, left: leftInset, bottom: bottomInset, right: rightInset)
        super.drawText(in: UIEdgeInsetsInsetRect(rect, insets))
    }
    
    override public var intrinsicContentSize: CGSize {
        var intrinsicSuperViewContentSize = super.intrinsicContentSize
        intrinsicSuperViewContentSize.height += topInset + bottomInset
        intrinsicSuperViewContentSize.width += leftInset + rightInset
        return intrinsicSuperViewContentSize
    }
}
