//
//  BLShadowView.swift
//  BLCoreFramework
//
//  Created by Muhammad Ikhsan on 15/11/17.
//  Copyright © 2017 BilineDEV. All rights reserved.
//

import UIKit

class BLShadowView: UIView {
    @IBInspectable var shadowOffset: CGSize = CGSize.zero {
        didSet {
            refreshView()
        }
    }
    @IBInspectable var shadowRadius: CGFloat = 0 {
        didSet {
            refreshView()
        }
    }
    @IBInspectable var shadowOpacity: Float = 0 {
        didSet {
            refreshView()
        }
    }
    @IBInspectable var shadowColor: UIColor = UIColor.gray {
        didSet {
            refreshView()
        }
    }
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            refreshView()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        backgroundColor = UIColor.clear
        layer.masksToBounds = false
    }
    
    func refreshView() {
        layer.shadowColor = shadowColor.cgColor
        layer.shadowOffset = shadowOffset
        layer.shadowRadius = shadowRadius
        layer.shadowOpacity = shadowOpacity
        layer.cornerRadius = cornerRadius
        
        setNeedsDisplay()
    }
    
    override func layoutIfNeeded() {
        super.layoutIfNeeded()
        layer.shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius).cgPath
    }
}
