//
//  BLCoreFramework.h
//  BLCoreFramework
//
//  Created by Muhammad Ikhsan on 14/11/17.
//  Copyright © 2017 BilineDEV. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for BLCoreFramework.
FOUNDATION_EXPORT double BLCoreFrameworkVersionNumber;

//! Project version string for BLCoreFramework.
FOUNDATION_EXPORT const unsigned char BLCoreFrameworkVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <BLCoreFramework/PublicHeader.h>


