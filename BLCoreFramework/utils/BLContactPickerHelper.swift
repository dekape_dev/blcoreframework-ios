//
//  BLContactPickerHelper.swift
//  BLCoreFramework
//
//  Created by Muhammad Ikhsan on 11/02/19.
//  Copyright © 2019 BilineDEV. All rights reserved.
//

import UIKit
import ContactsUI

protocol BLContactsPickerHelperDelegate: NSObjectProtocol {
    func contactHelperDidFound(contact: CNPhoneNumber)
}

class BLContactsPickerHelper: NSObject, CNContactPickerDelegate {
    
    private weak var controller: UIViewController?
    weak var delegate: BLContactsPickerHelperDelegate?
    
    init(controller: UIViewController) {
        self.controller = controller
    }
    
    func findContact() {
        guard let controller = controller else { return }
        let picker = CNContactPickerViewController()
        picker.displayedPropertyKeys = [CNContactGivenNameKey, CNContactFamilyNameKey, CNContactPhoneNumbersKey]
        picker.delegate = self
        picker.predicateForEnablingContact = NSPredicate(format: "phoneNumbers.@count > 0")
        picker.predicateForSelectionOfContact = NSPredicate(format: "phoneNumbers.@count == 1")
        picker.predicateForSelectionOfProperty = NSPredicate(format: "key == 'phoneNumbers'")
        findViewController(for: controller)?.present(picker, animated: true, completion: nil)
    }
    
    public func findViewController(for controller: UIViewController) -> UIViewController? {
        var nextResponder: UIResponder? = controller
        repeat {
            nextResponder = nextResponder?.next
            if let viewController = nextResponder as? UIViewController {
                return viewController
            }
        } while nextResponder != nil
        return nil
    }
    
    func contactPicker(_ picker: CNContactPickerViewController, didSelect contact: CNContact) {
        print(contact.phoneNumbers)
        guard let phoneNumber = contact.phoneNumbers.first?.value else { return }
        delegate?.contactHelperDidFound(contact: phoneNumber)
    }
    
    func contactPicker(_ picker: CNContactPickerViewController, didSelect contactProperty: CNContactProperty) {
        print(contactProperty)
        guard let phoneNumber = contactProperty.value as? CNPhoneNumber else { return }
        delegate?.contactHelperDidFound(contact: phoneNumber)
    }
}
