//
//  BLTableViewUtils.swift
//  BLCoreFramework
//
//  Created by Muhammad Ikhsan on 11/02/19.
//  Copyright © 2019 BilineDEV. All rights reserved.
//

import UIKit

typealias BLTableViewDelegate = UITableViewDelegate & UITableViewDataSource

extension UITableView {
    func setup(delegate: BLTableViewDelegate, estimatedRowHeight: CGFloat) {
        self.delegate = delegate
        self.dataSource = delegate
        self.rowHeight = UITableViewAutomaticDimension
        self.estimatedRowHeight = estimatedRowHeight
    }
    
    func setup(delegate: BLTableViewDelegate, rowHeight: CGFloat) {
        self.delegate = delegate
        self.dataSource = delegate
        self.rowHeight = rowHeight
    }
    
    func registerCell(_ cell: UITableViewCell.Type) {
        self.register(cell.NIB, forCellReuseIdentifier: cell.className)
    }
    
    func registerCells(_ cells: [UITableViewCell.Type]) {
        cells.forEach({ registerCell($0) })
    }
    
    func registerHeaderFooter(_ view: UITableViewHeaderFooterView.Type) {
        self.register(view.NIB, forHeaderFooterViewReuseIdentifier: view.className)
    }
    
    func reusableCell<T: UITableViewCell>(for indexPath: IndexPath) -> T {
        return dequeueReusableCell(withIdentifier: T.className, for: indexPath) as! T
    }
    
    func reusableCell<T: UITableViewCell>() -> T {
        return dequeueReusableCell(withIdentifier: T.className) as! T
    }
    
    func reusableHeaderFooterView<T: UITableViewHeaderFooterView>() -> T {
        return dequeueReusableHeaderFooterView(withIdentifier: T.className) as! T
    }
}

extension UITableViewCell {
    static var NIB: UINib {
        return UINib.init(nibName: self.className, bundle: Bundle.main)
    }
}

extension UITableViewHeaderFooterView {
    static var NIB: UINib {
        return UINib.init(nibName: self.className, bundle: Bundle.main)
    }
}
