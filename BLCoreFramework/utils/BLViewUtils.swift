//
//  UIViewUtils.swift
//  BLCoreFramework
//
//  Created by Muhammad Ikhsan on 14/11/17.
//  Copyright © 2017 BilineDEV. All rights reserved.
//

import Foundation

import UIKit

extension UIApplication {
    var appVersion: String {
        return Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String
    }
}

extension UIViewController {
    func wrappedInNavigationController() -> UINavigationController {
        return UINavigationController(rootViewController: self)
    }
}

extension UIView {
    static func viewNIB<T>(withClass type: T.Type) -> T {
        return Bundle.main.loadNibNamed(String(describing: type), owner: self, options: nil)?.first as! T
    }
    
    static func viewFromNIB<T>() -> T {
        return Bundle.main.loadNibNamed(String(describing: self), owner: self, options: nil)?.first as! T
    }
    
    static var className: String {
        return String(describing: self)
    }
}

extension UIButton {
    func addClick(on observer: Any, action: Selector) {
        addTarget(observer, action: action, for: .touchUpInside)
    }
    
    func removeClick(on observer: Any, action: Selector) {
        removeTarget(observer, action: action, for: .touchUpInside)
    }
}

extension UIView {
    @discardableResult
    func enableTap(on: Any, with selector: Selector) -> UITapGestureRecognizer {
        isUserInteractionEnabled = true
        let tapGesture = UITapGestureRecognizer(target: on, action: selector)
        addGestureRecognizer(tapGesture)
        return tapGesture
    }
    
    func disableTap(gesture: UITapGestureRecognizer) {
        removeGestureRecognizer(gesture)
    }
}

extension UIControl {
    func addOnValueChanged(on observer: Any, action: Selector) {
        addTarget(observer, action: action, for: .valueChanged)
    }
    
    func removeOnValueChanged(on observer: Any, action: Selector) {
        removeTarget(observer, action: action, for: .valueChanged)
    }
}

extension UIBarButtonItem {
    static func createCustomViewButton() -> UIBarButtonItem {
        let size: CGFloat = 25
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: size, height: size))
        button.tintColor = .white
        button.widthAnchor.constraint(equalToConstant: size).isActive = true
        button.heightAnchor.constraint(equalToConstant: size).isActive = true
        button.imageView?.contentMode = .scaleAspectFit
        return UIBarButtonItem(customView: button)
    }
}
