//
//  BLViewControllerUtils.swift
//  BLCoreFramework
//
//  Created by Muhammad Ikhsan on 14/11/17.
//  Copyright © 2017 BilineDEV. All rights reserved.
//

import Foundation

extension UIViewController {
    
    func popController() {
        if self.navigationController != nil {
            _ = self.navigationController!.popViewController(animated: true)
        }
    }
    
    func popToRootController() {
        if self.navigationController != nil {
            _ = self.navigationController!.popToRootViewController(animated: true)
        }
    }
    
    func showInfoFeatureNotAvailable() {
        showInfo(on: self, message: "Fitur dalam pengembangan")
    }
}

func showError(on vc: UIViewController, title: String? = "Error", error: Error?, actionTitle: String? = "OK", action: ((UIAlertAction) -> Void)? = nil) {
    showInfo(on: vc, title: title, message: error!.localizedDescription, actionTitle: actionTitle, action: action)
}

func showInfo(on vc: UIViewController, title: String? = nil, message: String, actionTitle: String? = "OK", action: ((UIAlertAction) -> Void)? = nil) {
    let info = UIAlertController(title: title, message: message, preferredStyle: .alert)
    info.addAction(UIAlertAction(title: actionTitle!, style: .cancel, handler: action))
    vc.present(info, animated: true, completion: nil)
}
