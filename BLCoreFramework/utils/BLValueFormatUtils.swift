//
//  BLValueFormatUtils.swift
//  BLCoreFramework
//
//  Created by Muhammad Ikhsan on 11/02/19.
//  Copyright © 2019 BilineDEV. All rights reserved.
//

import Foundation

class ValueFormatUtils {
    
    private let dateFormatter: DateFormatter!
    private let numberFormatter: NumberFormatter!
    
    init() {
        dateFormatter = DateFormatter()
        numberFormatter = NumberFormatter()
    }
    
    func numberToCurrency(value: Double?) -> String {
        let formatter = numberFormatter
        formatter?.numberStyle = .currencyISOCode
        formatter?.currencyCode = "Rp "
        formatter?.alwaysShowsDecimalSeparator = false
        formatter?.maximumFractionDigits = 0
        return formatter?.string(from: NSNumber(value: value ?? 0)) ?? "Rp 0"
    }
    
    func numberToCurrency(value: String?) -> String {
        guard let double = Double(value ?? "0") else { return "Rp 0" }
        return numberToCurrency(value: double)
    }
    
    func numberToCurrency(value: Int?) -> String {
        return numberToCurrency(value: Double(value ?? 0))
    }
    
    func numberFormatted(value: Double) -> String {
        let formatter = numberFormatter
        formatter?.numberStyle = .decimal
        formatter?.alwaysShowsDecimalSeparator = false
        formatter?.maximumFractionDigits = 0
        return formatter?.string(from: NSNumber(value: value)) ?? "0"
    }
    
    func readableTrxDate(format: String = "dd/MM/yy'\n'HH:mm:ss", value: String) -> String {
        let formatter = dateFormatter
        formatter?.dateFormat = "yyyy-MM-dd'T'HH:mm:ss'.000Z'"
        guard let date = formatter?.date(from: value) else { return "-" }
        formatter?.dateFormat = format
        return formatter?.string(from: date) ?? "-"
    }
    
    func readableDate(format: String, value: Date) -> String {
        let formatter = dateFormatter
        formatter?.dateFormat = format
        return formatter?.string(from: value) ?? "-"
    }
    
    func readableDate(toFormat: String, fromFormat: String, value: String) -> String {
        let formatter = dateFormatter
        formatter?.dateFormat = fromFormat
        guard let date = formatter?.date(from: value) else { return "-" }
        formatter?.dateFormat = toFormat
        return formatter?.string(from: date) ?? "-"
    }
}
