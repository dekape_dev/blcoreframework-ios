//
//  BLCollectionViewUtils.swift
//  BLCoreFramework
//
//  Created by Muhammad Ikhsan on 11/02/19.
//  Copyright © 2019 BilineDEV. All rights reserved.
//

import UIKit

typealias BLCollectionViewDelegate = UICollectionViewDelegate & UICollectionViewDataSource & UICollectionViewDelegateFlowLayout

extension UICollectionReusableView {
    static var NIB: UINib {
        return UINib.init(nibName: self.className, bundle: Bundle.main)
    }
}

enum CollectionViewSupplementaryKind {
    case header
    case footer
    
    var value: String {
        switch self {
        case .header:
            return UICollectionElementKindSectionHeader
        case .footer:
            return UICollectionElementKindSectionFooter
        }
    }
}

extension UICollectionView {
    func setup(delegate: BLCollectionViewDelegate) {
        self.delegate = delegate
        self.dataSource = delegate
    }
    
    func registerCell(_ cell: UICollectionViewCell.Type) {
        self.register(cell.NIB, forCellWithReuseIdentifier: cell.className)
    }
    
    func registerCells(_ cells: [UICollectionViewCell.Type]) {
        cells.forEach({ self.registerCell($0) })
    }
    
    func registerCellByClass(_ cell: UICollectionViewCell.Type) {
        self.register(cell.self, forCellWithReuseIdentifier: cell.className)
    }
    
    func registerSupplementaryView(ofKind: String, view: UICollectionReusableView.Type) {
        self.register(view.NIB, forSupplementaryViewOfKind: ofKind, withReuseIdentifier: view.className)
    }
    
    func registerSupplementaryView(ofKind: CollectionViewSupplementaryKind, view: UICollectionReusableView.Type) {
        self.register(view.NIB, forSupplementaryViewOfKind: ofKind.value, withReuseIdentifier: view.className)
    }
    
    func reusableCell<T: UICollectionViewCell>(for indexPath: IndexPath) -> T {
        return dequeueReusableCell(withReuseIdentifier: T.className, for: indexPath) as! T
    }
    
    func reusableSupplementaryView<T: UICollectionReusableView>(ofKind: String, for indexPath: IndexPath) -> T {
        return dequeueReusableSupplementaryView(ofKind: ofKind, withReuseIdentifier: T.className, for: indexPath) as! T
    }
    
    func reusableSupplementaryView<T: UICollectionReusableView>(ofKind: CollectionViewSupplementaryKind, for indexPath: IndexPath) -> T {
        return dequeueReusableSupplementaryView(ofKind: ofKind.value, withReuseIdentifier: T.className, for: indexPath) as! T
    }
}
