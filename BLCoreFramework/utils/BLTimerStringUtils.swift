//
//  BLTimerStringUtils.swift
//  BLCoreFramework
//
//  Created by Muhammad Ikhsan on 14/11/17.
//  Copyright © 2017 BilineDEV. All rights reserved.
//

import Foundation

extension DateComponents {
    func timerFormattedString() -> String {
        if self.second! < 0 || self.minute! < 0 || self.hour! < 0 {
            return "00:00:00"
        } else {
            var hour = "\(self.hour!)"
            hour = hour.count < 2 ? "0\(hour)" : hour
            var minute = "\(self.minute!)"
            minute = minute.count < 2 ? "0\(minute)" : minute
            var second = "\(self.second!)"
            second = second.count < 2 ? "0\(second)" : second
            return "\(hour):\(minute):\(second)"
        }
    }
    
    func isExpired() -> Bool {
        return self.second! < 0 || self.minute! < 0 || self.hour! < 0
    }
}
